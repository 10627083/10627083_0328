package sample;

import com.speedment.runtime.core.ApplicationBuilder;
import database.MydbApplication;
import database.MydbApplicationBuilder;
import database._.schema.city.City;
import database._.schema.city.CityImpl;
import database._.schema.city.CityManager;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;

import java.util.ArrayList;
import java.util.List;

public class Controller {
    public Button btnDB;
    public ComboBox cbBirth;

    public void DoConn(ActionEvent actionEvent) {
        MydbApplication mydbApplication = new MydbApplicationBuilder().withLogging(ApplicationBuilder.LogType.CONNECTION)
                .withLogging(ApplicationBuilder.LogType.STREAM).withLogging(ApplicationBuilder.LogType.PERSIST)
                .withLogging(ApplicationBuilder.LogType.UPDATE).build();
        CityManager cityManager = mydbApplication.getOrThrow(CityManager.class);
        cityManager.stream().forEach(System.out::println);
        City city = new CityImpl().setCityname(123).setRowid(cityManager.stream().count()+1);
        cityManager.persist(city);

        List<String> citys = new ArrayList<>();
        cityManager.stream().forEach(e-> {
            if (e.getCityname().isPresent()) {
                citys.add(e.getCityname().get());

            }
        });
    ObservableList<String> observableList = FXCollections.observableList(citys);
    cbBirth.setItems()
            }
        }
    }
}
