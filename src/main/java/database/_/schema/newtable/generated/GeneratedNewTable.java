package database._.schema.newtable.generated;

import com.speedment.common.annotation.GeneratedCode;
import com.speedment.enterprise.datastore.runtime.field.DatastoreFields;
import com.speedment.runtime.config.identifier.ColumnIdentifier;
import com.speedment.runtime.config.identifier.TableIdentifier;
import com.speedment.runtime.core.util.OptionalUtil;
import com.speedment.runtime.field.ComparableField;
import com.speedment.runtime.field.LongField;
import com.speedment.runtime.typemapper.TypeMapper;
import database._.schema.newtable.NewTable;

import java.util.OptionalInt;

/**
 * The generated base for the {@link
 * database._.schema.newtable.NewTable}-interface representing entities of the
 * {@code NewTable}-table in the database.
 * <p>
 * This file has been automatically generated by Speedment. Any changes made to
 * it will be overwritten.
 * 
 * @author Speedment
 */
@GeneratedCode("Speedment")
public interface GeneratedNewTable {
    
    /**
     * This Field corresponds to the {@link NewTable} field that can be obtained
     * using the {@link NewTable#getSex()} method.
     */
    ComparableField<NewTable, Integer, Integer> SEX = DatastoreFields.createComparableField(
        Identifier.SEX,
        o -> OptionalUtil.unwrap(o.getSex()),
        NewTable::setSex,
        TypeMapper.identity(),
        false
    );
    /**
     * This Field corresponds to the {@link NewTable} field that can be obtained
     * using the {@link NewTable#getPlace()} method.
     */
    ComparableField<NewTable, Integer, Integer> PLACE = DatastoreFields.createComparableField(
        Identifier.PLACE,
        o -> OptionalUtil.unwrap(o.getPlace()),
        NewTable::setPlace,
        TypeMapper.identity(),
        false
    );
    /**
     * This Field corresponds to the {@link NewTable} field that can be obtained
     * using the {@link NewTable#getBirthday()} method.
     */
    ComparableField<NewTable, Integer, Integer> BIRTHDAY = DatastoreFields.createComparableField(
        Identifier.BIRTHDAY,
        o -> OptionalUtil.unwrap(o.getBirthday()),
        NewTable::setBirthday,
        TypeMapper.identity(),
        false
    );
    /**
     * This Field corresponds to the {@link NewTable} field that can be obtained
     * using the {@link NewTable#getBlood()} method.
     */
    ComparableField<NewTable, Integer, Integer> BLOOD = DatastoreFields.createComparableField(
        Identifier.BLOOD,
        o -> OptionalUtil.unwrap(o.getBlood()),
        NewTable::setBlood,
        TypeMapper.identity(),
        false
    );
    /**
     * This Field corresponds to the {@link NewTable} field that can be obtained
     * using the {@link NewTable#getName()} method.
     */
    ComparableField<NewTable, Integer, Integer> NAME = DatastoreFields.createComparableField(
        Identifier.NAME,
        o -> OptionalUtil.unwrap(o.getName()),
        NewTable::setName,
        TypeMapper.identity(),
        false
    );
    /**
     * This Field corresponds to the {@link NewTable} field that can be obtained
     * using the {@link NewTable#getSid()} method.
     */
    ComparableField<NewTable, Integer, Integer> SID = DatastoreFields.createComparableField(
        Identifier.SID,
        o -> OptionalUtil.unwrap(o.getSid()),
        NewTable::setSid,
        TypeMapper.identity(),
        false
    );
    /**
     * This Field corresponds to the {@link NewTable} field that can be obtained
     * using the {@link NewTable#getRowid()} method.
     */
    LongField<NewTable, Long> ROWID = DatastoreFields.createLongField(
        Identifier.ROWID,
        NewTable::getRowid,
        NewTable::setRowid,
        TypeMapper.primitive(),
        true
    );
    
    /**
     * Returns the sex of this NewTable. The sex field corresponds to the
     * database column .schema.NewTable.sex.
     * 
     * @return the sex of this NewTable
     */
    OptionalInt getSex();
    
    /**
     * Returns the place of this NewTable. The place field corresponds to the
     * database column .schema.NewTable.place.
     * 
     * @return the place of this NewTable
     */
    OptionalInt getPlace();
    
    /**
     * Returns the birthday of this NewTable. The birthday field corresponds to
     * the database column .schema.NewTable.birthday.
     * 
     * @return the birthday of this NewTable
     */
    OptionalInt getBirthday();
    
    /**
     * Returns the blood of this NewTable. The blood field corresponds to the
     * database column .schema.NewTable.blood.
     * 
     * @return the blood of this NewTable
     */
    OptionalInt getBlood();
    
    /**
     * Returns the name of this NewTable. The name field corresponds to the
     * database column .schema.NewTable.name.
     * 
     * @return the name of this NewTable
     */
    OptionalInt getName();
    
    /**
     * Returns the sid of this NewTable. The sid field corresponds to the
     * database column .schema.NewTable.sid.
     * 
     * @return the sid of this NewTable
     */
    OptionalInt getSid();
    
    /**
     * Returns the rowid of this NewTable. The rowid field corresponds to the
     * database column .schema.NewTable.rowid.
     * 
     * @return the rowid of this NewTable
     */
    long getRowid();
    
    /**
     * Sets the sex of this NewTable. The sex field corresponds to the database
     * column .schema.NewTable.sex.
     * 
     * @param sex to set of this NewTable
     * @return    this NewTable instance
     */
    NewTable setSex(Integer sex);
    
    /**
     * Sets the place of this NewTable. The place field corresponds to the
     * database column .schema.NewTable.place.
     * 
     * @param place to set of this NewTable
     * @return      this NewTable instance
     */
    NewTable setPlace(Integer place);
    
    /**
     * Sets the birthday of this NewTable. The birthday field corresponds to the
     * database column .schema.NewTable.birthday.
     * 
     * @param birthday to set of this NewTable
     * @return         this NewTable instance
     */
    NewTable setBirthday(Integer birthday);
    
    /**
     * Sets the blood of this NewTable. The blood field corresponds to the
     * database column .schema.NewTable.blood.
     * 
     * @param blood to set of this NewTable
     * @return      this NewTable instance
     */
    NewTable setBlood(Integer blood);
    
    /**
     * Sets the name of this NewTable. The name field corresponds to the
     * database column .schema.NewTable.name.
     * 
     * @param name to set of this NewTable
     * @return     this NewTable instance
     */
    NewTable setName(Integer name);
    
    /**
     * Sets the sid of this NewTable. The sid field corresponds to the database
     * column .schema.NewTable.sid.
     * 
     * @param sid to set of this NewTable
     * @return    this NewTable instance
     */
    NewTable setSid(Integer sid);
    
    /**
     * Sets the rowid of this NewTable. The rowid field corresponds to the
     * database column .schema.NewTable.rowid.
     * 
     * @param rowid to set of this NewTable
     * @return      this NewTable instance
     */
    NewTable setRowid(long rowid);
    
    enum Identifier implements ColumnIdentifier<NewTable> {
        
        SEX      ("sex"),
        PLACE    ("place"),
        BIRTHDAY ("birthday"),
        BLOOD    ("blood"),
        NAME     ("name"),
        SID      ("sid"),
        ROWID    ("rowid");
        
        private final String columnId;
        private final TableIdentifier<NewTable> tableIdentifier;
        
        Identifier(String columnId) {
            this.columnId        = columnId;
            this.tableIdentifier = TableIdentifier.of(    getDbmsId(), 
                getSchemaId(), 
                getTableId());
        }
        
        @Override
        public String getDbmsId() {
            return "";
        }
        
        @Override
        public String getSchemaId() {
            return "schema";
        }
        
        @Override
        public String getTableId() {
            return "NewTable";
        }
        
        @Override
        public String getColumnId() {
            return this.columnId;
        }
        
        @Override
        public TableIdentifier<NewTable> asTableIdentifier() {
            return this.tableIdentifier;
        }
    }
}