package database._.schema.newtable.generated;

import com.speedment.common.annotation.GeneratedCode;
import com.speedment.runtime.config.identifier.TableIdentifier;
import com.speedment.runtime.core.manager.Manager;
import com.speedment.runtime.field.Field;
import database._.schema.newtable.NewTable;

import java.util.List;

import static java.util.Arrays.asList;
import static java.util.Collections.unmodifiableList;

/**
 * The generated base interface for the manager of every {@link
 * database._.schema.newtable.NewTable} entity.
 * <p>
 * This file has been automatically generated by Speedment. Any changes made to
 * it will be overwritten.
 * 
 * @author Speedment
 */
@GeneratedCode("Speedment")
public interface GeneratedNewTableManager extends Manager<NewTable> {
    
    TableIdentifier<NewTable> IDENTIFIER = TableIdentifier.of("", "schema", "NewTable");
    List<Field<NewTable>> FIELDS = unmodifiableList(asList(
        NewTable.ROWID,
        NewTable.SEX,
        NewTable.PLACE,
        NewTable.BIRTHDAY,
        NewTable.BLOOD,
        NewTable.NAME,
        NewTable.SID
    ));
    
    @Override
    default Class<NewTable> getEntityClass() {
        return NewTable.class;
    }
}