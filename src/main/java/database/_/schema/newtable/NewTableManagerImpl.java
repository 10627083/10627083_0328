package database._.schema.newtable;

import database._.schema.newtable.generated.GeneratedNewTableManagerImpl;

/**
 * The default implementation for the manager of every {@link
 * database._.schema.newtable.NewTable} entity.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author company
 */
public final class NewTableManagerImpl 
extends GeneratedNewTableManagerImpl 
implements NewTableManager {}